%{?cygwin_package_header}

Name:		cygwin-pcre
Version:	8.40
Release:	1%{?dist}
Summary:	Cygwin pcre library

Group:		Development/Libraries
License:	BSD
URL:		http://www.pcre.org/
BuildArch:	noarch

Source0:	ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-%{version}.tar.bz2
# Upstream thinks RPATH is good idea.
Patch0:     pcre-8.21-multilib.patch
# Refused by upstream, bug #675477
#Patch1:     pcre-8.32-refused_spelling_terminated.patch

# Cygwin-specific
Patch100:   pcre-8.39-cygwin-jit.patch

BuildRequires:  autoconf, automake, libtool

BuildRequires:	cygwin32-filesystem
BuildRequires:	cygwin32-gcc
BuildRequires:	cygwin32-gcc-c++
BuildRequires:	cygwin32-binutils

BuildRequires:	cygwin64-filesystem
BuildRequires:	cygwin64-gcc
BuildRequires:	cygwin64-gcc-c++
BuildRequires:	cygwin64-binutils

%description
Cross compiled PCRE library for use with Cygwin toolchains.

%package -n cygwin32-pcre
Summary:        PCRE library for Cygwin32 toolchain
Group:          Development/Libraries

%description -n cygwin32-pcre
Cross compiled PCRE library for use with Cygwin i686 toolchain.

%package -n cygwin64-pcre
Summary:        PCRE library for Cygwin64 toolchain
Group:          Development/Libraries

%description -n cygwin64-pcre
Cross compiled PCRE library for use with Cygwin x86_64 toolchain.

%{?cygwin_debug_package}


%prep
%setup -q -n pcre-%{version}
%patch0 -p1
#patch1 -p1
%patch100 -p2
mkdir -p m4
autoreconf -fiv

# One contributor's name is non-UTF-8
for F in ChangeLog; do
    iconv -f latin1 -t utf8 "$F" >"${F}.utf8"
    touch --reference "$F" "${F}.utf8"
    mv "${F}.utf8" "$F"
done

%build
%cygwin_configure \
	--enable-shared --disable-static \
	--enable-jit \
	--enable-pcre16 --enable-pcre32 \
	--enable-utf8 --enable-unicode-properties \
	--enable-newline-is-anycrlf
%cygwin_make %{?_smp_mflags}

%install
%cygwin_make install DESTDIR=$RPM_BUILD_ROOT

# Remove unnecessary Cygwin native binaries
rm -f $RPM_BUILD_ROOT%{cygwin32_bindir}/*.exe
rm -f $RPM_BUILD_ROOT%{cygwin64_bindir}/*.exe

# We intentionally don't ship *.la files
find $RPM_BUILD_ROOT -name '*.la' -delete

# same documentation as from native pcre
rm -rf $RPM_BUILD_ROOT%{cygwin32_docdir}
rm -rf $RPM_BUILD_ROOT%{cygwin32_mandir}
rm -rf $RPM_BUILD_ROOT%{cygwin64_docdir}
rm -rf $RPM_BUILD_ROOT%{cygwin64_mandir}


%files -n cygwin32-pcre
%doc AUTHORS COPYING LICENCE NEWS README ChangeLog
%{cygwin32_bindir}/pcre-config
%{cygwin32_bindir}/cygpcre-1.dll
%{cygwin32_bindir}/cygpcre16-0.dll
%{cygwin32_bindir}/cygpcre32-0.dll
%{cygwin32_bindir}/cygpcrecpp-0.dll
%{cygwin32_bindir}/cygpcreposix-0.dll
%{cygwin32_includedir}/pcre*.h
%{cygwin32_libdir}/libpcre.dll.a
%{cygwin32_libdir}/libpcre16.dll.a
%{cygwin32_libdir}/libpcre32.dll.a
%{cygwin32_libdir}/libpcrecpp.dll.a
%{cygwin32_libdir}/libpcreposix.dll.a
%{cygwin32_libdir}/pkgconfig/libpcre.pc
%{cygwin32_libdir}/pkgconfig/libpcre16.pc
%{cygwin32_libdir}/pkgconfig/libpcre32.pc
%{cygwin32_libdir}/pkgconfig/libpcrecpp.pc
%{cygwin32_libdir}/pkgconfig/libpcreposix.pc

%files -n cygwin64-pcre
%doc AUTHORS COPYING LICENCE NEWS README ChangeLog
%{cygwin64_bindir}/pcre-config
%{cygwin64_bindir}/cygpcre-1.dll
%{cygwin64_bindir}/cygpcre16-0.dll
%{cygwin64_bindir}/cygpcre32-0.dll
%{cygwin64_bindir}/cygpcrecpp-0.dll
%{cygwin64_bindir}/cygpcreposix-0.dll
%{cygwin64_includedir}/pcre*.h
%{cygwin64_libdir}/libpcre.dll.a
%{cygwin64_libdir}/libpcre16.dll.a
%{cygwin64_libdir}/libpcre32.dll.a
%{cygwin64_libdir}/libpcrecpp.dll.a
%{cygwin64_libdir}/libpcreposix.dll.a
%{cygwin64_libdir}/pkgconfig/libpcre.pc
%{cygwin64_libdir}/pkgconfig/libpcre16.pc
%{cygwin64_libdir}/pkgconfig/libpcre32.pc
%{cygwin64_libdir}/pkgconfig/libpcrecpp.pc
%{cygwin64_libdir}/pkgconfig/libpcreposix.pc


%changelog
* Tue Dec 05 2017 Yaakov Selkowitz <yselkowi@redhat.com> - 8.40-1
- new version

* Mon Sep 12 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 8.39-1
- new version
- enable JIT

* Wed Mar 30 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 8.38-1
- new version with latest Fedora patchset

* Fri Jun 28 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 8.32-2
- Rebuild for new Cygwin packaging scheme.
- Add cygwin64 support.

* Sun Jan 20 2013 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 8.32-1
- Version bump.
- Add libpcre32.

* Wed May 23 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 8.30-1
- Version bump; breaks ABI compatibility in libpcre.
- Add libpcre16.

* Tue Jan 10 2012 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 8.21-1
- Version bump.

* Thu Feb 17 2011 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 8.12-1
- Initial spec file, largely based on mingw32-pcre.
